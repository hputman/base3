<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Person;
use Cake\I18N\Time;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Entity\Person Test Case
 */
class PersonTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Person = new Person([
                'id' => 1,
                'firstname' => 'Simon',
                'lastname' => 'Blargo',
                'token' => '0123456789ABCDEF',
                'token_expires' => null,
                'subscribed' => null,
                'unsubscribed' => null,
                'created' => '2015-08-30 06:30:00',
                'modified' => '2015-08-30 06:30:00']);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Person);
        Time::setTestNow();

        parent::tearDown();
    }

    /**
     * Test tokenExpired method
     *
     * @return void
     */
    public function testTokenExpired()
    {
        $this->assertTrue($this->Person->tokenExpired(), "Unset token is expired.");

        // Update the token
        $token = $this->Person->token;
        $this->Person->updateToken();
        $this->assertNotEquals($token, $this->Person->token, "Token has new value");
        $this->assertFalse($this->Person->tokenExpired(), "Updated token is not expired right away.");

        Time::setTestNow(new Time('+ 3 months'));
        $this->assertTrue($this->Person->tokenExpired(), "In the future, token will expire.");
    }

    /**
     * Test updateToken method
     *
     * @return void
     */
    public function testUpdateToken()
    {
        Time::setTestNow(new Time("2015-08-07 18:23:45"));
        $this->Person->updateToken("1 hour");
        $this->assertEquals(new Time("2015-08-07 19:23:45"), $this->Person->token_expires);
    }

    public function testSubscribe()
    {
        $token = $this->Person->updateToken("5 minutes");
        Time::setTestNow(Time::now());
        $result = $this->Person->subscribe($token);
        $this->assertTrue($result, 'Operation succeeded');
        $this->assertEquals(Time::now(), $this->Person->subscribed, 'Subscribed set');
    }

    public function testSubscribeBadToken()
    {
        $result = $this->Person->subscribe('definitely-bad-token');
        $this->assertFalse($result, 'Operation failed');
        $this->assertNull($this->Person->subscribed, 'Person not subscribed');
    }

    public function testSubscribeExpiredToken()
    {
        $oldPassword = $this->Person->password;
        $token = $this->Person->updateToken('5 minutes');
        Time::setTestNow(new Time('+ 6 minutes'));
        $result = $this->Person->subscribe($token);
        $this->assertFalse($result, 'Operation failed');
        $this->assertNull($this->Person->subscribed, 'Person not subscribed');
    }
}
