<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\User;
use Cake\I18N\Time;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Entity\User Test Case
 */
class UserTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->User = new User([
            'id' => 1,
            'username' => 'blargo@test.com',
            'password' => 'some-long-hashed-value',
            'role' => 'user',
            'token' => '0123456789ABCDEF',
            'token_expires' => null,
            'visited' => null,
            'created' => '2015-08-30 06:30:00',
            'modified' => '2015-08-30 06:30:00']);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->User);
        Time::setTestNow();

        parent::tearDown();
    }

    /**
     * Test tokenExpired method
     *
     * @return void
     */
    public function testTokenExpired()
    {
        $this->assertTrue($this->User->tokenExpired(), "Unset token is expired.");

        // Update the token
        $token = $this->User->token;
        $this->User->updateToken();
        $this->assertNotEquals($token, $this->User->token, "Token has new value");
        $this->assertFalse($this->User->tokenExpired(), "Updated token is not expired right away.");

        Time::setTestNow(new Time('+ 3 months'));
        $this->assertTrue($this->User->tokenExpired(), "In the future, token will expire.");
    }

    /**
     * Test updateToken method
     *
     * @return void
     */
    public function testUpdateToken()
    {
        Time::setTestNow(new Time("2015-08-07 18:23:45"));
        $this->User->updateToken("1 hour");
        $this->assertEquals(new Time("2015-08-07 19:23:45"), $this->User->token_expires);
    }

    public function testPasswordHashed()
    {
        $this->User->set('password', 'sesame');
        $this->assertNotRegExp('/sesame/', $this->User->password, "Password not stored in the clear.");
    }

    public function testUsernameStoredLowercase()
    {
        $this->User->set('username', 'Sm0kYDopE@COLORWAND.mq7.COM');
        $this->assertEquals('sm0kydope@colorwand.mq7.com', $this->User->username, "Username stored in lowercase.");
    }

    public function testChangePassword()
    {
        $oldPassword = $this->User->password;
        $token = $this->User->updateToken("5 minutes");
        $result = $this->User->resetPassword('bananas', $token);
        $this->assertTrue($result, 'Operation succeeded');
        $newPassword = $this->User->password;
        $this->assertNotRegExp('/bananas/', $newPassword, "Password not stored in the clear.");
        $this->assertNotEquals($oldPassword, $newPassword, 'Password changed');
    }

    public function testPwdBadToken()
    {
        $oldPassword = $this->User->password;
        $result = $this->User->resetPassword('pizza', 'definitely-bad-token');
        $this->assertFalse($result, 'Operation failed');
        $newPassword = $this->User->password;
        $this->assertEquals($oldPassword, $newPassword, 'Password not changed');
    }

    public function testPwdExpiredToken()
    {
        $oldPassword = $this->User->password;
        $token = $this->User->updateToken('5 minutes');
        Time::setTestNow(new Time('+ 6 minutes'));
        $result = $this->User->resetPassword('pizza', $token);
        $this->assertFalse($result, 'Operation failed');
        $newPassword = $this->User->password;
        $this->assertEquals($oldPassword, $newPassword, 'Password not changed');
    }
}
