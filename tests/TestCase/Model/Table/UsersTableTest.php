<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersTable;
use Cake\I18N\Time;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Users') ? [] : ['className' => 'App\Model\Table\UsersTable'];
        $this->Users = TableRegistry::get('Users', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Users);
        Time::setTestNow();
        parent::tearDown();
    }

    public function testFindUser()
    {
        $found = $this->Users->findByUsername('DoLOR@amet.IT')->first();
        $this->assertEquals(2, $found->id, 'Found it');
        $this->assertEquals('dolor@amet.it', $found->username, 'Stored in lower case');
    }

    public function testVisited()
    {
        $time = new Time('2015-10-23 16:00:00');
        Time::setTestNow($time);
        $this->Users->visited(['id' => 1]);
        $user = $this->Users->get(1);
        $this->assertEquals($time, $user->visited, 'Timestamp updated');
    }
}
