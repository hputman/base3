<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersController;
use Cake\I18N\Time;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\UsersController Test Case
 */
class UsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'username' => 'lorem@ipsum.com',
                    'role' => 'user'
                    // other keys.
                ]
            ]
        ]);
        $this->get(['controller' => 'Users', 'action' => 'index']);
        $this->assertResponseOk();
        $this->assertNoRedirect();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testCreateUserAndLogin()
    {
        $username = 'sample@test.com';
        $password = 'tr4inStat3';
        $this->post(
            ['controller' => 'Users', 'action' => 'add'],
            ['username' => $username, 'password' => $password]
        );
        $this->assertRedirect(['action' => 'index']);
        $this->post(
            ['controller' => 'Users', 'action' => 'login'],
            ['username' => $username, 'password' => $password]
        );
        $this->assertRedirect('/articles');
    }

    /**
     * Test failing login
     *
     * @return void
     */
    public function testFailedLogin()
    {
        $username = 'sample2@test.com';
        $password = 'tr4inStat3';
        $this->post(
            ['controller' => 'Users', 'action' => 'add'],
            ['username' => $username, 'password' => $password]
        );
        $this->post(
            ['controller' => 'Users', 'action' => 'login'],
            ['username' => $username, 'password' => 'not-my-password']
        );
        $this->assertNoRedirect();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testLoginDatestamp()
    {
        $timestamp = new Time('2015-10-26 22:48:15');
        Time::setTestNow($timestamp);

        $username = 'benheer@test.com';
        $password = 'cat5intr33s';
        $this->post(
            ['controller' => 'Users', 'action' => 'add'],
            ['username' => $username, 'password' => $password]
        );
        $this->assertRedirect(['action' => 'index']);

        $users = TableRegistry::get('Users');
        $user = $users->findByUsername($username)->first();
        $this->assertNull($user->visited);
        $this->post(
            ['controller' => 'Users', 'action' => 'login'],
            ['username' => $username, 'password' => $password]
        );
        $user = $users->findByUsername($username)->first();
        $this->assertEquals($timestamp, $user->visited, 'Last login time recorded');
    }
}
