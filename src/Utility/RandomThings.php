<?php
/**
 * Copyright 2010 - 2015, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2015, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
namespace App\Utility;

class RandomThings
{
    /**
     * Returns a cryptographicaly secure random number within a range.
     * @param int $min The minimum positive integer
     * @param int $min The maximum positive integer (inclusive)
     * @return The random value
     */
    public static function secureRand($min, $max)
    {
        $range = abs($max - $min);
        $min = min($min, $max);
        if ($range < 1) {
            return $min; // not so random...
        }
        $log = ceil(log($range, 2));
        $bytes = (int)($log / 8) + 1; // length in bytes
        $bits = (int)$log + 1; // length in bits
        $filter = (int)(1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    /**
     * Generates random string
     * Based on code found [here](https://gist.github.com/raveren/5555297)
     * and [here](https://gist.github.com/raveren/5555297)
     * @param int $length String size.
     * @return string
     */
    public static function randomString($length = 10, $type = 'alnum')
    {
        if (!is_numeric($length) || $length <= 0) {
            $length = 10;
        }
        $token = "";
        switch ($type) {
            case 'alnum':
                $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'alpha':
                $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'hexdec':
                $pool = '0123456789abcdef';
                break;
            case 'numeric':
                $pool = '0123456789';
                break;
            case 'nozero':
                $pool = '123456789';
                break;
            case 'distinct':
                $pool = '2345679ACDEFHJKLMNPRSTUVWXYZ';
                break;
            default:
                $pool = (string)$type;
                break;
        }
        $max = strlen($pool) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $pool[self::secureRand(0, $max)];
        }
        return $token;
    }
}
