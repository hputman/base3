<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\I18N\Time;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('username');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('username', 'A username is required');

        $validator
            ->notEmpty('password', 'A password is required');

        $validator
            ->add('role', 'inList', [
                'rule' => ['inList', ['admin', 'user', 'leader']],
                'message' => 'Please enter a valid role'
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        return $rules;
    }

    /**
     * Intercepts the dynamic finder so we can massage the username before
     * lookup. Convert to lowercase. Some other ideas might be to remove dots
     * from gmail addresses.
     * @param string $username The username to look up.
     * @return mixed The query builder
     */
    public function findByUsername($username)
    {
        $username = User::normalizeUsername($username);
        return $this->_dynamicFinder(__FUNCTION__, [$username]);
    }

    /**
     * Updates the date of last visit for the specified user.
     * @param mixed $user Array of user data, or userId
     */
    public function visited($user)
    {
        $userId = is_integer($user) ? $user : $user['id'];
        $this->updateAll(['visited' => Time::now()], ['id' => $userId]);
    }
}
