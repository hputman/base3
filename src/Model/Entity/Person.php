<?php
namespace App\Model\Entity;

use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\Entity;

/**
 * Person Entity.
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property string $token
 * @property \Cake\I18n\Time $token_expires
 * @property \Cake\I18n\Time $subscribed
 * @property \Cake\I18n\Time $unsubscribed
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Person extends Entity
{
    use TokenTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Subscribe the user
     * @return true if successful.
     */
    public function subscribe($token)
    {
        if ($this->tokenExpired()) {
            Log::error('Token expired. Password reset failed');
            return false;
        }
        if ($token != $this->token) {
            Log::error('Token mismatch. Password reset failed');
            return false;
        }
        $this->subscribed = Time::now();
        return true;
    }
}
