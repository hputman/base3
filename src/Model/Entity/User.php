<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\Entity;

/**
 * User Entity.
 */
class User extends Entity
{
    use TokenTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     * Note that '*' is set to true, which allows all unspecified fields to be
     * mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => false,
        'username' => true,
        'password' => true,
        'token' => true,
        'token_expires' => true,
        'activated' => true,
    ];

    /**
     * Hash the password when saving it.
     * @param string $password The password to save
     */
    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

    /**
     * Normalize the user name before storage and lookup so that equivalent
     * user names work. HaRolD@mail.com and harold@mail.com are the same.
     * @param string $username The name to Normalize
     * @return string The normalized name
     */
    public static function normalizeUsername($username)
    {
        return strtolower($username);
    }
    /**
     * Lowercase the username when saving it.
     * @param string $username The username to save
     */
    protected function _setUsername($username)
    {
        return self::normalizeUsername($username);
    }

    /**
     * Resets the user password
     * @return true if successful.
     */
    public function resetPassword($password, $token)
    {
        if ($this->tokenExpired()) {
            Log::error('Token expired. Password reset failed');
            return false;
        }
        if ($token != $this->token) {
            Log::error('Token mismatch. Password reset failed');
            return false;
        }
        $this->password = $password;
        return true;
    }
}
