<?php

namespace App\Model\Entity;

use App\Utility\RandomThings;
use Cake\I18N\Time;

trait TokenTrait
{
    /**
     * Returns if the token has already expired
     *
     * @return bool
     */
    public function tokenExpired()
    {
        return empty($this->token_expires) || Time::now()->gte(new Time($this->token_expires));
    }

    /**
     * Generate token_expires and token in a user
     * @param string $tokenExpiration How long token lasts.
     *
     * @return void
     */
    public function updateToken($tokenExpiration = '3 months')
    {
        $expires = new Time("+ $tokenExpiration");
        $this->token_expires = $expires;
        $this->token = RandomThings::randomString(16, 'distinct');
        return $this->token;
    }
}
